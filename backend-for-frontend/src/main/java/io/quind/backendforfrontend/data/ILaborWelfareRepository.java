package io.quind.backendforfrontend.data;

import org.springframework.http.ResponseEntity;

public interface ILaborWelfareRepository {

    ResponseEntity<String> add(Object employee);
}

