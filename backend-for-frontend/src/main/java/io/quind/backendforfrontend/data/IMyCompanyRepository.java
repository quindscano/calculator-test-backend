package io.quind.backendforfrontend.data;

import io.quind.backendforfrontend.business.Employee;
import org.springframework.http.ResponseEntity;

public interface IMyCompanyRepository {

    ResponseEntity<String> add(Employee employee);

    ResponseEntity<String> remove(Employee employee);
}

