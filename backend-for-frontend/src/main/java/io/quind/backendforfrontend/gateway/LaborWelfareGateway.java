package io.quind.backendforfrontend.gateway;

import io.quind.backendforfrontend.data.ILaborWelfareRepository;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Repository
public class LaborWelfareGateway implements ILaborWelfareRepository {

    private static final String MY_COMPANY_API = "http://localhost:8081/employees/";

    @Override
    public ResponseEntity<String> add(Object employee) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<Object> request = new HttpEntity<>(employee);

        try {
            return restTemplate.exchange(MY_COMPANY_API, HttpMethod.POST, request, String.class);
        }catch (HttpClientErrorException e){
            return new ResponseEntity<>(e.getStatusCode());
        }catch (Exception e){
            return new ResponseEntity<>("ERROR!", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
