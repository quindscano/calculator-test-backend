package io.quind.backendforfrontend.gateway;

import io.quind.backendforfrontend.business.Employee;
import io.quind.backendforfrontend.data.IMyCompanyRepository;
import org.json.JSONObject;
import org.springframework.http.*;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Repository
public class MyCompanyGateway implements IMyCompanyRepository {

    private static final String MY_COMPANY_API = "http://localhost:8080/employees/";
    private static final String MY_COMPANY_API_ID = "http://localhost:8080/employees/";


    @Override
    public ResponseEntity<String> add(Employee employee) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<Object> request = new HttpEntity<>(employee);
        JSONObject personJsonObject = new JSONObject();

        try {
            return restTemplate.exchange(MY_COMPANY_API, HttpMethod.POST, request, String.class);
        }catch (HttpClientErrorException e){
            return new ResponseEntity<>(e.getStatusCode());
        }
    }

    @Override
    public ResponseEntity<String> remove(Employee employee) {
        RestTemplate restTemplate = new RestTemplate();

        try {
            return restTemplate.exchange(MY_COMPANY_API_ID + employee.getId(), HttpMethod.DELETE,
                    new HttpEntity<Employee>(employee), String.class);
        } catch (HttpClientErrorException e) {
            return new ResponseEntity<>(e.getStatusCode());
        }
    }
}
