package io.quind.backendforfrontend.services;

import io.quind.backendforfrontend.business.Employee;
import io.quind.backendforfrontend.gateway.LaborWelfareGateway;
import io.quind.backendforfrontend.gateway.MyCompanyGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("employees")
public class EmployeeController {

    @Autowired
    private MyCompanyGateway myCompanyGateway;

    @Autowired
    private LaborWelfareGateway laborWelfareGateway;

    ResponseEntity<String> myCompanyAddEmployeeResponse;
    ResponseEntity<String> laborWelfareAddEmployeeResponse;
    ResponseEntity<String> myCompanyEmployeeRollbackResponse;

    @CrossOrigin
    @PostMapping
    public ResponseEntity<String> add(@RequestBody Employee newEmployee) {
        myCompanyAddEmployeeResponse = myCompanyGateway.add(newEmployee);

        if ("SAVE OK!".equals(myCompanyAddEmployeeResponse.getBody())) {
            laborWelfareAddEmployeeResponse = laborWelfareGateway.add(newEmployee);
        } else {
            return new ResponseEntity<>("SAVE ERROR MY COMPANY!", myCompanyAddEmployeeResponse.getStatusCode());
        }

        if ("SAVE OK!".equals(laborWelfareAddEmployeeResponse.getBody())) {
            return new ResponseEntity<>("SAVE OK!", HttpStatus.OK);
        } else {
            myCompanyEmployeeRollbackResponse = myCompanyGateway.remove(newEmployee);
        }

        if ("DELETE OK!".equals(myCompanyEmployeeRollbackResponse.getBody())) {
            return new ResponseEntity<>("SAVE ERROR LABOR WELFARE!", HttpStatus.CONFLICT);
        }
        else {
            return new ResponseEntity<>("SAVE ERROR LABOR WELFARE AND FAIL ROLLBACK IN MY COMPANY SYSTEM!",
                    myCompanyEmployeeRollbackResponse.getStatusCode() );
        }
    }

}



