package io.quind.calculator;

public class Calculator {

    public double sum(double... nums){
        double result = 0;
        for (int i=0 ; i < nums.length ; i++) {
            if (i==0) {
                result = nums[i];
            }
            else{
                result  += nums[i];
            }
        }
        return result;
    }

    public double sub(double... nums){
        double result = 0;
        for (int i=0 ; i < nums.length ; i++) {
            if (i==0) {
                result = nums[i];
            }
            else{
                result  -= nums[i];
            }
        }
        return result;
    }

    public double div(double... nums) throws Exception {
        double result = 0;
        for (int i=0 ; i < nums.length ; i++) {
            if (i==0) {
                result = nums[i];
            }
            else if (nums[i]!=0){
                result  /= nums[i];
            }else{
                throw new Exception("Zero divider not allowed");
            }
        }
        return result;
    }

    public double multi(double... nums){
        double result = 0;
        for (int i=0 ; i < nums.length ; i++) {
            if (i==0) {
                result = nums[i];
            }
            else{
                result  *= nums[i];
            }
        }
        return result;
    }
}
