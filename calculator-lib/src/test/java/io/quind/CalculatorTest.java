package io.quind;

import io.quind.calculator.Calculator;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CalculatorTest {

    private static final double DELTA = 1e-15;

    @Test
    public void sumOfTwoNumbersShouldReturnExpectedResult(){
        Calculator calculator = new Calculator();
        assertEquals(-15, calculator.sum(6, 10, -34, 0, 3), DELTA);
    }

    @Test
    public void subtractionOfTwoNumbersShouldReturnExpectedResult(){
        Calculator calculator = new Calculator();
        assertEquals(27, calculator.sub(6, 10, -34, 0, 3), DELTA);
    }


    @Test
    public void multiplicationOfTwoNumbersReturnExpectedResult(){
        Calculator calculator = new Calculator();
        assertEquals(-6120.000, calculator.multi(6, 10, -34, 1, 3), DELTA);
    }

    @Test
    public void multiplicationOfNumberForZeroReturnZero(){
        Calculator calculator = new Calculator();
        assertEquals(0, calculator.multi(6, 10, -34, 0, 3), DELTA);
    }

    @Test
    public void divisionOfTwoNumbersReturnExpectedResult() throws Exception {
        Calculator calculator = new Calculator();
        assertEquals(-3.5, calculator.div( 56, 8, -2), DELTA);
    }

    @Test(expected = Exception.class)
    public void divisionOfNumberForZeroReturnExceptionMessage() throws Exception {
        Calculator calculator = new Calculator();
        calculator.div(56, 8, 0);
    }

}
