package io.quind.calculatorsalary.data;

public interface ISalaryRepository {

    Double consultById(Long id);
}

