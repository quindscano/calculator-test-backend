package co.com.quind.certificacion.example.questions;

import co.com.quind.certificacion.example.models.Employee;
import co.com.quind.certificacion.example.utils.EmployeeGateway;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import org.springframework.http.HttpStatus;

public class ResponseCodeGetUserById implements Question<HttpStatus> {

    private String url;
    private Employee employee;

    public ResponseCodeGetUserById(String url, Employee employee) {
        this.url = url;
        this.employee = employee;
    }

    public static Question<HttpStatus> with(String url, Employee employee){
        return new ResponseCodeGetUserById(url, employee);
    }

    @Override
    public HttpStatus answeredBy(Actor actor) {
        return EmployeeGateway.consultEmployeeById(url, employee).getStatusCode();
    }

}
