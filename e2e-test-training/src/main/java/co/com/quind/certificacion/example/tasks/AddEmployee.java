package co.com.quind.certificacion.example.tasks;

import co.com.quind.certificacion.example.models.Employee;
import co.com.quind.certificacion.example.userinterfaces.EmployeesSystemApi;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Post;

public class AddEmployee implements Task {

    private Employee employee;

    public AddEmployee(Employee employee) {
    this.employee = employee;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Post.to(EmployeesSystemApi.EMPLOYEES_RESOURCE).with(
                request -> request.header("Content-Type", "application/json")
                        .body(employee)));
    }
    public static AddEmployee with(Employee employee) {
        return Tasks.instrumented(AddEmployee.class, employee);
    }

}