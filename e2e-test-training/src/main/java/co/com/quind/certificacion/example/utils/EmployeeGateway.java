package co.com.quind.certificacion.example.utils;

import co.com.quind.certificacion.example.models.Employee;
import org.json.JSONObject;
import org.springframework.http.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;


public class EmployeeGateway {

    public static void addWithInfo(String url, Employee employee){
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        JSONObject employeeJsonObject = new JSONObject(employee);
        HttpEntity<String> request = new HttpEntity<>(employeeJsonObject.toString(), headers);

        try {
            restTemplate.exchange(url, HttpMethod.POST, request, String.class);
        }catch (Exception e){
            throw e;
        }
    }

    public static void deleteWithInfo(String url, Employee employee) {
        RestTemplate restTemplate = new RestTemplate();

        try {
            restTemplate.delete(url + employee.getId());
        } catch (Exception e) {
            throw e;
        }
    }

    public static ResponseEntity<String> consultEmployeeById(String url, Employee employee) {
        String consultByIdUri = url + employee.getId();
        RestTemplate restTemplate = new RestTemplate();

        try {
            return restTemplate.getForEntity(consultByIdUri, String.class);
        }catch (HttpClientErrorException | HttpServerErrorException e){
            return new ResponseEntity<String>("ERROR", e.getStatusCode());
        }
    }

}
