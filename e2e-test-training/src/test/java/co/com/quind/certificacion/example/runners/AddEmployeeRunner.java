package co.com.quind.certificacion.example.runners;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features/add_employee.feature"
               , glue = "co.com.quind.certificacion.example.stepdefinitions"
               , snippets = SnippetType.CAMELCASE)

public class AddEmployeeRunner {

    private AddEmployeeRunner() {
    }
}
