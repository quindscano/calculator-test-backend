package co.com.quind.certificacion.example.stepdefinitions;

import co.com.quind.certificacion.example.models.Employee;
import co.com.quind.certificacion.example.questions.ResponseCodeGetUserById;
import co.com.quind.certificacion.example.tasks.AddEmployee;
import co.com.quind.certificacion.example.userinterfaces.EmployeesSystemApi;
import co.com.quind.certificacion.example.userinterfaces.LaborWelfareSystemApi;
import co.com.quind.certificacion.example.userinterfaces.MyCompanySystemApi;
import co.com.quind.certificacion.example.utils.EmployeeGateway;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import org.springframework.http.HttpStatus;

import java.util.List;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;

public class AddEmployeeStepDefinitions {

    private Employee employee;
    private Employee preEmployee;

    @Before(value="@test1")
    public void beforeSteps() {
        preEmployee = new Employee(1285463265,"Julian", "Perez", 4560000.2, 2);
        EmployeeGateway.addWithInfo(LaborWelfareSystemApi.URL + LaborWelfareSystemApi.EMPLOYEES_RESOURCE, preEmployee );

        OnStage.setTheStage(new OnlineCast());
    }

    @When("^Juan adds an employee that already exists in Labor Welfare System$")
    public void juanAddsAnEmployeeThatAlreadyExistsInLaborWelfareSystem(List<Employee> employees) {
        employee = employees.get(0);

        OnStage.theActorCalled("Juan").whoCan(CallAnApi.at(EmployeesSystemApi.URL));
        OnStage.theActorInTheSpotlight().attemptsTo(AddEmployee.with(employee));
    }


    @Then("^he should validate a an error message (.*)$")
    public void heShouldValidateAAnErrorMessage(String message) {
        OnStage.theActorInTheSpotlight().should(seeThatResponse(response -> response.statusCode(409).
                body(containsString(message))));
    }

    @Then("^that the employee was not saved in My Company System$")
    public void thatTheEmployeeWasNotSavedInMyCompanySystem() {
        OnStage.theActorInTheSpotlight().should(seeThat
                (ResponseCodeGetUserById.with(MyCompanySystemApi.URL+
                        MyCompanySystemApi.EMPLOYEES_RESOURCE,employee), equalTo(HttpStatus.NOT_FOUND)));
    }


    @When("^Juan adds an employee that already exists in Government System$")
    public void juanAddsAnEmployeeThatAlreadyExistsInGovernmentSystem(List<Employee> employees) {
        employee = employees.get(0);

        OnStage.theActorCalled("Juan").whoCan(CallAnApi.at(EmployeesSystemApi.URL));
        OnStage.theActorInTheSpotlight().attemptsTo(AddEmployee.with(employee));
    }

    @Then("^that the employee was not saved in My Company System and Labor Welfare System$")
    public void thatTheEmployeeWasNotSavedInMyCompanySystemAndLaborWelfareSystem() {
        OnStage.theActorInTheSpotlight().should(
                seeThat(ResponseCodeGetUserById.with(MyCompanySystemApi.URL+
                        MyCompanySystemApi.EMPLOYEES_RESOURCE,employee), equalTo(HttpStatus.NOT_FOUND)),
                seeThat(ResponseCodeGetUserById.with(LaborWelfareSystemApi.URL+
                        LaborWelfareSystemApi.EMPLOYEES_RESOURCE,employee), equalTo(HttpStatus.NOT_FOUND)));
    }

    @After(value="@test1")
    public void afterSteps() {
        EmployeeGateway.deleteWithInfo(LaborWelfareSystemApi.URL + LaborWelfareSystemApi.EMPLOYEES_RESOURCE, preEmployee );
    }
}
