Feature: Add Employee
 As commercial management
 I want to create employees
 in My Company and Labor Welfare Systems

 @test1
 Scenario: Employee already exists Labor Welfare System
  When Juan adds an employee that already exists in Labor Welfare System
   |id        |name  |lastName|baseSalary|score|
   |1285463265|Julian|Perez   |4560000.2 |2   |
  Then he should validate a an error message SAVE ERROR LABOR WELFARE!
  And that the employee was not saved in My Company System


 Scenario: Employee already exists Government System
  When Juan adds an employee that already exists in Government System
   |id        |name |lastName|baseSalary|score|
   |1285463259|Marco|Diaz    |8900000.2 |4    |
  Then he should validate a an error message SAVE ERROR MY COMPANY!
  And that the employee was not saved in My Company System and Labor Welfare System
